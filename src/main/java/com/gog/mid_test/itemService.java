/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gog.mid_test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class itemService {
    public static ArrayList<Item> itemList = null;
    static {
        itemList = new ArrayList<>();
        itemList.add(new Item(101,"Rice","Easy",15.0,1));
        itemList.add(new Item(102,"Sausage","Ceepee",35.0,1));
        itemList.add(new Item(103,"Snack","ArhanYodkhun",10.5,2));
    }
    public static Item getItem(int index){
        return itemList.get(index);
    }
    public static int totalprice(){
        int totalprice=0;
        for(int i =0;i<itemList.size();i++){
            totalprice += itemList.get(i).getPrice();
        }
        return totalprice;
    }
    public static int total(){
        int total=0;
        for(int i =0;i<itemList.size();i++){
            total += itemList.get(i).getAmount();
        }
        return total;
    }
    public static boolean addItem(Item item){
        itemList.add(item);
        return true;
    }
    
    public static boolean delItem(Item item){
        itemList.remove(item);
        return true;
    }
    
    public static boolean delAllItem(){
        for(int i =0;i<itemService.itemList.size();i++){    
            itemList.remove(delItem(i));
        }  
        return true;
    }
    
    public static boolean delItem(int index){
        itemList.remove(index);
        return true;
    }
    public static ArrayList<Item> getItem(){
        return itemList;
    }
    public static boolean updateItem(int index,Item item){
        itemList.set(index,item);
        return true;
    }
    public static void save(){
        File file = null;
            FileOutputStream fos = null;
            ObjectOutputStream oos = null;
        try {
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(itemList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(itemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(itemService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void load(){
        File file = null;
            FileInputStream fis = null;
            ObjectInputStream ois = null;
        try {
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            itemList = (ArrayList<Item>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(itemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(itemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(itemService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
